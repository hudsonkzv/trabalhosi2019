# Pacotes utilizados para criação do app
library(shiny)
library(shinydashboard)
library(shinyjs)
library(shinyBS)
library(shinycssloaders)
library(readxl)
library(dplyr)
library(ggplot2)


# Aba dados
dadosTab <-  tabItem("dados",
                           fluidRow(
                             column(width = 6,
                                    infoBox(
                                      width = NULL,
                                      icon = icon("heart"),
                                      title = h4("Média Hemoglobina"),
                                      subtitle = textOutput("media_hemoglobina",
                                                            inline = TRUE)
                                    )
                             ),
                             column(width = 6,
                                    infoBox(
                                      width = NULL,
                                      icon = icon("heart"),
                                      title = h4("Média Albumina Sérica"),
                                      subtitle = textOutput("media_albuSer",
                                                            inline = TRUE)
                                    )
                             ),
                             column(width = 6,
                                    infoBox(
                                      width = NULL,
                                      icon = icon("heart"),
                                      title = h4("Média Fósforo"),
                                      subtitle = textOutput("media_fosforo",
                                                            inline = TRUE)
                                    )
                             ),
                             div(id = "tabela1", box( 
                               color='olive',
                               width = 12, 
                               status = "success", 
                               solidHeader = TRUE,
                               title = "Tabela Pessoa",
                               collapsible = TRUE,
                               dataTableOutput("tabela1")  
                             )
                             )
                           )
)

# Aba gráficos
graficosTab <-  tabItem("graficos",
                     fluidRow(
                       div(id = "grafico1", box( 
                         width = 12, 
                         status = "info", 
                         solidHeader = TRUE,
                         title = "Gráfico Pessoas por Idade",
                         collapsible = TRUE,
                         plotOutput("grafico1")
                       )),
                       div(id = "grafico2", box( 
                         width = 12, 
                         status = "info", 
                         solidHeader = TRUE,
                         title = "Gráfico Pessoas por Sexo",
                         collapsible = TRUE,
                         plotOutput("grafico2")
                       )),
                       div(id = "grafico3", box( 
                         width = 12, 
                         status = "info", 
                         solidHeader = TRUE,
                         title = "Gráfico Pressão Sistólica",
                         collapsible = TRUE,
                         plotOutput("grafico3")
                       )),
                       div(id = "grafico4", box( 
                         width = 12, 
                         status = "info", 
                         solidHeader = TRUE,
                         title = "Gráfico Pressão Diastólica",
                         collapsible = TRUE,
                         plotOutput("grafico4")
                       )),
                       div(id = "grafico5", box( 
                         width = 12, 
                         status = "info", 
                         solidHeader = TRUE,
                         title = "Hemoglobina por Idade",
                         collapsible = TRUE,
                         plotOutput("grafico5")
                       )),
                       div(id = "grafico6", box( 
                         width = 12, 
                         status = "info", 
                         solidHeader = TRUE,
                         title = "Pressão Diastólica por Peso",
                         collapsible = TRUE,
                         plotOutput("grafico6")
                       ))
                    
                     ) 
)

# Configuração da dashboard
dashboardPage(skin = "black",
  
  dashboardHeader(title = "Trabalho SI"),
  
  dashboardSidebar(
    useShinyjs(),
    sidebarMenu(id = "tabs",
                menuItem("Dados", tabName = "dados", icon = icon("home")),
                # Conjunto de inputs da tab home, inicialmente escondidos
                div(id = "dados_tab_input",
                           selectInput(
                             inputId ="tabela_filtro", 
                             label = "Tabela : ",
                             choices = c("Pessoa", "Hospital", "Prontuario", "Auditor"),
                             selected = "Pessoa"
                           ),
                           selectInput(
                             inputId ="sexo_filtro", 
                             label = "Sexo : ",
                             choices = c("Masculino", "Feminino", "Ambos"),
                             selected = "Ambos"
                           ),
                           sliderInput("filtro_idade", label = h3("Idade"), min = 18,
                           max = 89, value = c(30, 60))
                ),
                menuItem("Gráficos", tabName = "graficos", icon = icon("map")),
                # Conjunto de inputs da tab mapas, inicialmente escondidos
                hidden(div(id = "mapas_tab_input",
                           selectInput(
                             inputId ="mapas_filtro", 
                             label = "Mapas : ",
                             choices = c("Por UF", "Por safd", "Por Masdf"),
                             selected = "Por UF"
                           )
                )
                ))
  ),
  
  dashboardBody(
    tags$head(tags$link(rel = "stylesheet", type = "text/css", href = "custom.css")),
    useShinyjs(),
    tabItems(
      dadosTab,
      graficosTab
    )
  )
)

